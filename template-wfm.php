<?php
/**
 * Template Name: Winter Farmers' Market
 *
 * @package seacoast
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-8" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'wfm' );

				// If comments are open or we have at least one comment, load up the comment template.
				//if ( comments_open() || get_comments_number() ) :
				//	comments_template();
				//endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar( 'wfm' );
get_footer();
