/**
 * SEACOAST
 * http://underscores.me
 *
 * Theme javascript
 */

(function ($) {
  'use strict';

  	// make wp_nav_menu compatible with bootstrap dropdowns
	$( "#main-primary-menu li.menu-item-has-children" ).addClass( "dropdown" );
	$( "#main-primary-menu li.menu-item-has-children > a").addClass( "dropdown-toggle" );
	$( "#main-primary-menu li.menu-item-has-children > a").attr('data-toggle', 'dropdown');
	$( "#main-primary-menu ul.sub-menu" ).addClass( "dropdown-menu" );
	$( "#main-primary-menu ul.sub-menu" ).addClass( "dropdown-menu-left" );

  // search
  // mobile
  $( ".search-glass-mobile a").click( function () {
		$(".search-mobile").toggle( 400);
    $(".search-form .search-field").focus();
	});
  // desktop
	$( "li.search-glass a").click( function () {
		$(".search-desktop").toggle( 600);
    $(".search-form .search-field").focus();
    return (false);
	});

  // mobile
	$( "footer .subscribe").click( function () {
		$(".subscribe-mobile").toggle( 300);
    //$(".search-form .search-field").focus();
    return (false);
	});
  // desktop
	$( "header .subscribe").click( function () {
		$(".subscribe-desktop").toggle( 300);
    //$(".search-form .search-field").focus();
    return (false);
	});

})(jQuery);
