<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package seacoast
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'seacoast' ); ?></a>

	<div class="container mobile visible-xs visible-sm navbar-fixed-top">
		<header id="masthead" class="site-header row" role="banner">
			<!-- mobile -->
			<div class="visible-xs visible-sm navbar navbar-default mobile col-xs-2 col-sm-2">
				<div class="navbar-header" style="float:left">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
			</div>
			<div class="visible-xs visible-sm col-xs-8 col-sm-8 site-branding mobile text-center" style="">
				<?php	// Try to retrieve the Custom Logo
					if ( has_custom_logo()): ?>
						<?php the_custom_logo(); ?>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<div class="visible-xs visible-sm navbar navbar-default col-xs-2 col-sm-2">
				<div class="search-glass-mobile">
					<a><i class="fa fa-search"></i></a>
				</div>
			</div>
			<nav class="visible-xs visible-sm mobile navbar navbar-default col-xs-12 col-sm-12" role="navigation">
				<?php
				$menu_args = array(
				"theme_location" => "primary",
				"container_class" => "navbar-collapse collapse",
				"menu_class" => "nav navbar-nav",
				"menu_id" => "main-primary-menu",
				);
				wp_nav_menu( $menu_args);
				?>
			</nav>



		<div class="col-xs-12 col-sm-12 text-left search-mobile" style="display:none;">
			<?php echo get_search_form(); ?>
		</div>

		</header><!-- #masthead -->
		<!-- <hr class="hidden-xs" /> -->
	</div><!-- end container -->

<div class="container desktop visible-md visible-lg">
	<header id="masthead" class="site-header row" role="banner">

		<!-- desktop -->
		<div class="hidden-xs hidden-sm site-branding col-sm-3">
			<?php	// Try to retrieve the Custom Logo
				if ( has_custom_logo()): ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="home" rel="home"><?php the_custom_logo(); ?></a>
			<?php endif; ?>
		</div><!-- .site-branding -->


		<div class="hidden-xs  hidden-sm col-sm-9">

			<div class="row">
				<div class="col-sm-12 text-right">
					<?php get_template_part( 'template-parts/content', 'social-links' ); ?>
				</div>
				<nav class="navbar navbar-default col-sm-12" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<?php
					$menu_args = array(
					"theme_location" => "primary",
					"container_class" => "navbar-collapse collapse",
					"menu_class" => "nav navbar-nav",
					"menu_id" => "main-primary-menu",
					);
					wp_nav_menu( $menu_args);
					?>
				</nav>
				<div class="col-sm-12 text-center">
					<div class="row">
						<div class="col-xs-6 text-center subscribe">
							<a href="<?php echo esc_url( home_url( '/subscribe' )); ?>"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;&nbsp; Subscribe</a>
						</div>
						<div class="col-xs-6 text-center donate">
							<a href="<?php echo esc_url( home_url( '/get-involved/donate' ) ); ?>">DONATE TODAY</a>
						</div>
					</div>
				</div>

		</div>
	</div>

	<div class="col-md-12 search-desktop" style="display:none;">
		<?php echo get_search_form();?>
	</div>

	<div class="col-xs-12 col-md-12 subscribe-desktop" style="display:none;">
		<?php	get_template_part( 'template-parts/form', 'mail-chimp' ); ?>
	</div>

	</header><!-- #masthead -->
	<!-- <hr class="hidden-xs" /> -->
</div><!-- end container -->

<div class="container">
	<div id="content" class="site-content row">
