<?php

function program_init() {
	register_taxonomy( 'program', array( 'sponsor' ), array(
		'hierarchical'      => true,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Programs', 'seacoast' ),
			'singular_name'              => _x( 'Program', 'taxonomy general name', 'seacoast' ),
			'search_items'               => __( 'Search Programs', 'seacoast' ),
			'popular_items'              => __( 'Popular Programs', 'seacoast' ),
			'all_items'                  => __( 'All Programs', 'seacoast' ),
			'parent_item'                => __( 'Parent Program', 'seacoast' ),
			'parent_item_colon'          => __( 'Parent Program:', 'seacoast' ),
			'edit_item'                  => __( 'Edit Program', 'seacoast' ),
			'update_item'                => __( 'Update Program', 'seacoast' ),
			'add_new_item'               => __( 'New Program', 'seacoast' ),
			'new_item_name'              => __( 'New Program', 'seacoast' ),
			'separate_items_with_commas' => __( 'Separate Programs with commas', 'seacoast' ),
			'add_or_remove_items'        => __( 'Add or remove Programs', 'seacoast' ),
			'choose_from_most_used'      => __( 'Choose from the most used Programs', 'seacoast' ),
			'not_found'                  => __( 'No Programs found.', 'seacoast' ),
			'menu_name'                  => __( 'Programs', 'seacoast' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'program',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'program_init' );
