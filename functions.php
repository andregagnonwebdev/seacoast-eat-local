<?php
/**
 * SEACOAST functions and definitions
 *
 * @package SEACOAST
 */

define( 'LOCAL_IP_ADDR', '10.0.2.15');
define( 'DISALLOW_FILE_EDIT', true);


if ( ! function_exists( 'seacoast_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function seacoast_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on SEACOAST, use a find and replace
	 * to change 'seacoast' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'seacoast', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'seacoast' ),
		'footer' => __( 'Footer Menu', 'seacoast' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// custom logo
	add_theme_support( 'custom-logo', array(
		 'height'      => 200,
		 'width'       => 200,
	) );

	// custom images
	//add_image_size( 'home-card', 600, 600, false);
	add_image_size( 'home-card', 600, 600, array( 'center', 'top'));
	add_image_size( 'sponsor', 160, 120, false);

}
endif; // seacoast_setup
add_action( 'after_setup_theme', 'seacoast_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
	 * @global int $content_width
	 */
	function seacoast_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'seacoast_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'seacoast_content_width', 0 );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function seacoast_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'seacoast' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar Home', 'seacoast' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar Winter Farmers\'s Market', 'seacoast' ),
		'id'            => 'sidebar-wfm',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'seacoast_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function seacoast_scripts() {

	if ( WP_DEBUG)
	{
		$version = 'V4'.date( '.YmdGis'); // for DEBUG
		//error_log( $version);
		//error_log( sprintf( "%s\n", $version), 3, '/var/www/sel2/error.log');
	}
	else
		$version = null;

	// WP required CSS
  wp_enqueue_style( 'seacoast-style', get_stylesheet_uri(), array(), $version ); // style.css

  //
	// // bootstrap css framework
	// wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css-dist/bootstrap.min.css', array(), $version);
	// // site CSS
	// $css = ( WP_DEBUG) ? '/css/main.css' : '/css-dist/main.min.css';
	// wp_enqueue_style( 'main', get_template_directory_uri() . $css, array( 'seacoast-style', 'bootstrap'), $version);

	if ( WP_DEBUG) {
		// bootstrap css framework
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), $version);
		// site CSS
		$css = '/css/main.css';
		wp_enqueue_style( 'main', get_template_directory_uri() . $css, array( 'seacoast-style', 'bootstrap'), $version);

	}
	else {
		// minified, concatenated CSS
		$css = '/css-dist/all.min.css';
		wp_enqueue_style( 'all', get_template_directory_uri() . $css, array( 'seacoast-style'), $version);
	}
	// font icons
	wp_enqueue_style( 'font-awesome', 'https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), null);



	// load google fonts

	$fonts = seacoast_get_theme_fonts();
	global $seacoast_all_fonts;
	foreach ($fonts as $f) {
		if ( array_key_exists( $f, $seacoast_all_fonts) && $seacoast_all_fonts[ $f][ 1]) {
			$l = 'https://fonts.googleapis.com/css?family=';
			global $seacoast_all_fonts;
			$l .= str_replace( ' ', '+', $f) . ':' . $seacoast_all_fonts[ $f][ 1];
			$l .= '';
			wp_enqueue_style( 'google-font-'.str_replace( ' ', '-', $f), $l, array( 'seacoast-style'), null);
		}
	}

  	// move jquery to footer
  if (!is_admin()) {
        wp_deregister_script('jquery');
        // Load the copy of jQuery that comes with WordPress in to footer
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, $version, true);
        wp_enqueue_script('jquery');
	}

	// for watch in Gruntfile.js
	if ( in_array( $_SERVER['SERVER_ADDR'], array( '127.0.0.1', LOCAL_IP_ADDR)) || pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) == 'dev') {
    	wp_enqueue_script( 'livereload', '//localhost:35729/livereload.js', '', false, true );
	}

	// use minified js
	$js = ( WP_DEBUG) ? 'bootstrap.js' : 'bootstrap.min.js';
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/'.$js, array('jquery'), $version, true);

	$js = ( WP_DEBUG) ? '/js/theme.js' : '/js/dist/theme.min.js';
  wp_enqueue_script( 'seacoast-theme-js', get_template_directory_uri() . $js, array('jquery'), $version, true );

	// new fontawesome
	//wp_enqueue_script( 'font-awesome-js', 'https://use.fontawesome.com/c88288cbb6.js', array(), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'seacoast_scripts' );

/**
 * Remove emoji support
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';
/**
 * Custom google fonts
 */
//require get_template_directory() . '/inc/google-font.php';

/**
 * Custom theme options
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom theme options
 */
//require get_template_directory() . '/inc/custom-theme-options.php';

/**
 * Custom nav walker
 */
require get_template_directory() . '/inc/custom-nav-walker.php';


/**
 * Utility functions
 */
// set up this global for some functions
$sep = '/';
$dl_filepath = dirname(dirname(dirname(dirname(__FILE__)))) . $sep . 'wp-content' . $sep . 'uploads';
require get_template_directory() . '/inc/util.php';

/**
 * Contact Form class
 */
//require get_template_directory() . '/inc/class.contact.php';

/**
 * Custom post types
 */
require get_template_directory() . '/taxonomies/program.php';
require get_template_directory() . '/post-types/sponsor.php';


/**
 * WooCommerce Support
 */
// Removes showing results in Storefront theme
//remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
// remove sort order in products display
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

//remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
//remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

//add_action('woocommerce_before_main_content', 'seacoast_theme_wrapper_start', 10);
//add_action('woocommerce_after_main_content', 'seacoast_theme_wrapper_end', 10);

function seacoast_theme_wrapper_start() {
    echo '<div id="primary" class="content-area">';
    echo '<div id="main" class="site-main col-xs-12 col-md-12" role="main">';

}

function seacoast_theme_wrapper_end() {
    echo '</div>';
    echo '</div>';
}

//add_action( 'after_setup_theme', 'seacoast_woocommerce_support' );
function seacoast_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// remove ellipsis
add_filter('relevanssi_ellipsis', 'seacoast_ellipsis_excerpt_function');
//function seacoast_ellipsis_excerpt_function($content, $post, $query) {
		function seacoast_ellipsis_excerpt_function() {
	return '';
}
// filter search results excerpt
add_filter('relevanssi_excerpt', 'seacoast_search_result_excerpt_function');
function seacoast_search_result_excerpt_function( $content ) {
				$s = $content[0];
				if ( ctype_upper( $s ) === false )
					$content = '...&nbsp;' . $content;

				$content .= '&nbsp;...';
	return $content;
}
