<?php

function sponsor_init() {
	register_post_type( 'sponsor', array(
		'labels'            => array(
			'name'                => __( 'Sponsors', 'seacoast' ),
			'singular_name'       => __( 'Sponsor', 'seacoast' ),
			'all_items'           => __( 'All Sponsors', 'seacoast' ),
			'new_item'            => __( 'New Sponsor', 'seacoast' ),
			'add_new'             => __( 'Add New', 'seacoast' ),
			'add_new_item'        => __( 'Add New Sponsor', 'seacoast' ),
			'edit_item'           => __( 'Edit Sponsor', 'seacoast' ),
			'view_item'           => __( 'View Sponsor', 'seacoast' ),
			'search_items'        => __( 'Search Sponsors', 'seacoast' ),
			'not_found'           => __( 'No Sponsors found', 'seacoast' ),
			'not_found_in_trash'  => __( 'No Sponsors found in trash', 'seacoast' ),
			'parent_item_colon'   => __( 'Parent Sponsor', 'seacoast' ),
			'menu_name'           => __( 'Sponsors', 'seacoast' ),
		),
		'public'            => false,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => false,
		'supports'          => array( 'title', 'editor', 'page-attributes' ),
		'has_archive'       => false,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-clipboard',
		'show_in_rest'      => true,
		'rest_base'         => 'sponsor',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'sponsor_init' );

function sponsor_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['sponsor'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Sponsor updated. <a target="_blank" href="%s">View Sponsor</a>', 'seacoast'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'seacoast'),
		3 => __('Custom field deleted.', 'seacoast'),
		4 => __('Sponsor updated.', 'seacoast'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Sponsor restored to revision from %s', 'seacoast'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Sponsor published. <a href="%s">View Sponsor</a>', 'seacoast'), esc_url( $permalink ) ),
		7 => __('Sponsor saved.', 'seacoast'),
		8 => sprintf( __('Sponsor submitted. <a target="_blank" href="%s">Preview Sponsor</a>', 'seacoast'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Sponsor scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Sponsor</a>', 'seacoast'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Sponsor draft updated. <a target="_blank" href="%s">Preview Sponsor</a>', 'seacoast'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'sponsor_updated_messages' );
