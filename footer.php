<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package seacoast
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info row">

			<?php if ( $s = seacoast_get_theme_mod( 'seacoast-mission-statement')): ?>
				<div class="col-xs-12 text-justified mission">
					<p>
						<?php echo $s ?>
					</p>
				</div>
			<?php endif; ?>

			<div class="col-xs-12 text-center donate">
				<a href="/get-involved/donate">DONATE TODAY</a>
			</div>

			<div class="col-xs-12 visible-xs visible-sm text-center subscribe">
				<a href=""><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;&nbsp; Subscribe</a>
			</div>

			<div class="col-xs-12 subscribe-mobile" style="display:none;">
				<?php	get_template_part( 'template-parts/form', 'mail-chimp' ); ?>
			</div>

			<div class="col-xs-12 text-center">
				<?php get_template_part( 'template-parts/content', 'social-links' ); ?>
			</div>
			<div class="col-xs-12 text-left pages">

				<?php
				$menu_args = array(
				"theme_location" => "footer",
				"container_class" => "",
				"menu_class" => "nav navbar-navx row",
				"menu_id" => "footer-menu",
				"walker" => new Nav_MainMenu_Walker(),
				);
				wp_nav_menu( $menu_args);
				?>

			</div>

			<div class="col-xs-12 text-center copyright">
				<?php if ( $s = seacoast_get_theme_mod( 'seacoast-company-name')): ?>
			    <?php echo $s ?><br  />
			  <?php endif; ?>
				<?php if ( $s = seacoast_get_theme_mod( 'seacoast-address')): ?>
			    <?php echo $s ?><br  />
			  <?php endif; ?>
				<?php if ( $s = seacoast_get_theme_mod( 'seacoast-town-state-zip')): ?>
			    <?php echo $s ?><br  />
			  <?php endif; ?>
				<?php if ( $s = seacoast_get_theme_mod( 'seacoast-telephone')): ?>
			    <?php echo $s ?><br  />
			  <?php endif; ?>
				<?php if ( $s = seacoast_get_theme_mod( 'seacoast-email')): ?>
			    <a href="mailto:<?php echo $s ?>"><?php echo $s; ?></a><br  />
			  <?php endif; ?>

				<?php if ( $s = seacoast_get_theme_mod( 'seacoast-copyright-message')): ?>
					&nbsp;<br  />
			    <?php echo $s ?>
			  <?php endif; ?>
				<div class="col-xs-12 text-right credit">
					<a href="https://andregagnon.com">Website by Andre Gagnon</a>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
</div> <!-- end container -->

<?php wp_footer(); ?>

</body>
</html>
