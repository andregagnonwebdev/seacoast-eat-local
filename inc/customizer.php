<?php
/**
 * SEACOAST Theme Customizer
 *
 * @package SEACOAST
 */


function seacoast_theme_mod_default( $key)
{
    static $seacoast_theme_mod_defaults = array(
      'seacoast-color-nav-link' => '#006393',
      'seacoast-color-text-heading' => '#006393',
      'seacoast-color-text-body' => '#000000',
      'seacoast-color-text-link' => '#5b8201',
      //'seacoast-color-text-footer' => '#000000',
      //'seacoast-color-text-footer-link' => '#B7D968',
      //'seacoast-color-text-footer-rollover' => '#B7D968',
      'seacoast-color-call-to-action' => '#FFAA5A',
      'seacoast-color-rule-lines' => '#CDE0EA',

      //'seacoast-color-background-body' => '#ffffff',
      //'seacoast-color-background-content' => '#efe9e5',

      'seacoast-font-header' =>  'Arial',
      'seacoast-font-nav-menu' =>  'Arial',
      //'seacoast-font-title' =>  'Arial',
      'seacoast-font-heading' =>  'Georgia',
      'seacoast-font-body-text' =>  'Arial',
      'seacoast-font-caption' =>  'Arial',
      'seacoast-font-button' =>  'Arial',
      'seacoast-font-footer' =>  'Arial',


      'seacoast-facebook' => '',
      'seacoast-instagram' => '',
      'seacoast-twitter' => '',
      'seacoast-pinterest' => '',
      'seacoast-flickr' => '',

      'seacoast-company-name' => 'Seacoast Eat Local',
      'seacoast-address' => '2 Washington Street, Suite 331',
      'seacoast-address-2' => '',
      'seacoast-town-state-zip' => 'Dover, NH 03820',
      'seacoast-telephone' => '888-600-0128',
      'seacoast-email' => 'jill@seacoasteatlocal.org',

      'seacoast-copyright-message' => '© Copyright 2010-2017. All rights reserved.',

      'seacoast-mission-statement' => 'Seacoast Eat Local connects people with sources of locally grown food.',

      'seacoast-img-upload' => '',   // footer

    );

    if ( array_key_exists($key, $seacoast_theme_mod_defaults) )
        return( $seacoast_theme_mod_defaults[ $key]);
    else
    {
        return( '');
    }
}

function seacoast_get_theme_mod( $key, $default='unused')
{
    // provide defaults for 2nd parameter
    return( get_theme_mod( $key, seacoast_theme_mod_default( $key)));
}


// return all google fonts used in theme options, used in functions.php
function seacoast_get_theme_fonts() {

    $fonts = array();

    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-header');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-nav-menu');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-title');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-heading');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-body-text');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-button');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-caption');
    $fonts[] = seacoast_get_theme_mod( 'seacoast-font-footer');
    $fonts = array_unique( $fonts);

    return( $fonts);
}


/////////////////////////////
// new font stuff



static $seacoast_all_fonts = array(
   'Arial' => array( 'serif', ''),
   'Arial' => array( 'sans-serif', ''),

   // google
   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Muli'  => array( 'sans-serif', '400,400i,700,700i'),
   'Puritan'  => array( 'sans-serif', '400,400i,700,700i'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto Condensed'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 static $seacoast_body_text_fonts = array(
   'Arial' => array( 'serif', ''),
   'Arial' => array( 'Helvetica, sans-serif', ''),

   // google
   'Lato'  => array( 'sans-serif', '400,400i,700,700i'),
   'Muli'  => array( 'sans-serif', '400,400i,700,700i'),
   'Puritan'  => array( 'sans-serif', '400,400i,700,700i'),
   'Raleway'  => array( 'sans-serif', '400,400i,700,700i'),
   'Roboto Condensed'  => array( 'sans-serif', '400,400i,700,700i'),

 );

 // Add Google fonts CSS to admin header
 function seacoast_load_fonts() {

 global $seacoast_all_fonts;

 }

 // load fonts for admin?  done in functions.php
 //add_action('customize_controls_enqueue_scripts', 'stout_oak_load_fonts');
 //add_action('wp_enqueue_scripts', 'seacoast_load_fonts');
 add_action('wp_default_scripts', 'seacoast_load_fonts');
 //add_action('admin_enqueue_scripts', 'seacoast_load_fonts');

 // Add Google font CSS to admin header
 function seacoast_font_family( $key) {

     global $seacoast_all_fonts;
     return( "'".$key."'".", ".$seacoast_all_fonts[ $key][0]);
}


/////////////////////////////
// old font stuff
function seacoast_google_fonts_filter( $f)
{
//    loco_print_r( $f);
    // remove some fonts
    return( $f['category'] != 'handwriting');
}

function seacoast_get_google_fonts( $max = 5)
{

  $googleAllFontList = array();
  global $seacoast_all_fonts;
  global $seacoast_body_text_fonts;

  $fonts = ($all ?  ($seacoast_all_fonts):($seacoast_body_text_fonts));
  foreach( $fonts as $key => $f) {
    $googleAllFontList[ $key] = $key;
  }
  return( $googleAllFontList);
}

function seacoast_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function seacoast_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
 //   $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    // remove some stuff
    $wp_customize->remove_control( 'header_textcolor' );
    $wp_customize->remove_panel( 'widgets');
    $wp_customize->remove_section( 'static_front_page');


    ///////////////////////////////////////////////////////////////////////////
    // Colors
    $wp_customize->add_setting( 'seacoast-color-nav-link',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-nav-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-nav-link',
        array(
        'label' => __( 'Navigation Menu', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );


    $wp_customize->add_setting( 'seacoast-color-text-body',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-text-body'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-text-body',
        array(
        'label' => __( 'Body Text', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'seacoast-color-text-link',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-text-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-text-link',
        array(
        'label' => __( 'Link', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

    /*
    $wp_customize->add_setting( 'seacoast-color-text-footer',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-text-footer'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-text-footer',
        array(
        'label' => __( 'Footer Text', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );
    $wp_customize->add_setting( 'seacoast-color-text-footer-link',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-text-footer-link'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-text-footer-link',
        array(
        'label' => __( 'Footer Link', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'seacoast-color-text-footer-rollover',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-text-footer-rollover'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-text-footer-rollover',
        array(
        'label' => __( 'Footer Link Rollover', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

*/

    $wp_customize->add_setting( 'seacoast-color-rule-lines',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-rule-lines'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-rule-lines',
        array(
        'label' => __( 'Rule Lines', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'seacoast-color-call-to-action',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-call-to-action'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-call-to-action',
        array(
        'label' => __( 'Call to Action Button', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

    /*

    $wp_customize->add_setting( 'seacoast-color-background',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-color-background'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-color-background',
        array(
        'label' => __( 'Background', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );

    $wp_customize->add_setting( 'seacoast-xxx',
        array(
        'type' => 'theme_mod',
        'default' => seacoast_theme_mod_default( 'seacoast-xxx'),
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'seacoast-xxx',
        array(
        'label' => __( 'XXX Color', 'seacoast_textdomain' ),
        'section' => 'colors',
    ) ) );
    */


    ///////////////////////////////////////////////////////////////////////////
    // Fonts

    $fontListAll = seacoast_get_google_fonts( 25);
    $fontListBodyText = seacoast_get_google_fonts( 25, false);

    // section
    $wp_customize->add_section( 'seacoast-font-setting', array(
            'title'          => 'Fonts',
            'priority'       => 20,
            'description' => '<b>' . __( 'Select fonts.', 'seacoast' ) .'</b>' ,
        ) );

    // setting/control
    $wp_customize->add_setting( 'seacoast-font-nav-menu', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-nav-menu'),
    //            'transport' => 'postMessage',
            'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'seacoast-font-nav-menu', array(
            'label' => 'Navigation Menu',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-nav-menu',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );
/*
    $wp_customize->add_setting( 'seacoast-font-title', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-title'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
        ) );

    $wp_customize->add_control( 'seacoast-font-title', array(
            'label' => 'Title',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-title',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );
*/
    $wp_customize->add_setting( 'seacoast-font-heading', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-heading'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'seacoast-font-heading', array(
            'label' => 'Heading',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-heading',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'seacoast-font-body-text', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-body-text'),
            //            'transport' => 'postMessage',
                        'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'seacoast-font-body-text', array(
            'label' => 'Body Text',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-body-text',
            'type'    => 'select',
            'choices' => $fontListBodyText,
    ) );

    $wp_customize->add_setting( 'seacoast-font-caption', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-caption'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'seacoast-font-caption', array(
            'label' => 'Caption',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-caption',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    $wp_customize->add_setting( 'seacoast-font-button', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-button'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'seacoast-font-button', array(
            'label' => 'Button',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-button',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

/*

    $wp_customize->add_setting( 'seacoast-font-footer', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-font-footer'),
            'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'seacoast-font-footer', array(
            'label' => 'Footer',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-footer',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );


    $wp_customize->add_setting( 'seacoast-font-xxx', array(
            'type'    => 'theme_mod',
            'default' => 'arial,helvetica',
            'transport' => 'postMessage',
    ) );

    $wp_customize->add_control( 'seacoast-font-xxx', array(
            'label' => 'Button Font',
            'section' => 'seacoast-font-setting',
            'settings'   => 'seacoast-font-xxx',
            'type'    => 'select',
            'choices' => $fontListAll,
    ) );

    */


    ///////////////////////////////////////////////////////////////////////////
    // Social Media
    $wp_customize->add_section( 'seacoast-social-media-settings', array(
        'title'          => 'Social Media Settings',
        'priority'       => 160,
        'description' => '<b>' . __( 'Social media links.' ) .'</b>' ,
    ) );



    $wp_customize->add_setting( 'seacoast-facebook', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'seacoast_sanitize_text',
    ) );
    $wp_customize->add_control( 'seacoast-facebook', array(
        'label' => __( 'Facebook URL', 'loco_textdomain' ),
        'section' => 'seacoast-social-media-settings',
        'settings'   => 'seacoast-facebook',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'seacoast-instagram', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'seacoast_sanitize_text',
    ) );
    $wp_customize->add_control( 'seacoast-instagram', array(
        'label' => __( 'Instagram URL', 'loco_textdomain' ),
        'section' => 'seacoast-social-media-settings',
        'settings'   => 'seacoast-instagram',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'seacoast-twitter', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'seacoast_sanitize_text',
    ) );
    $wp_customize->add_control( 'seacoast-twitter', array(
        'label' => __( 'Twitter URL', 'loco_textdomain' ),
        'section' => 'seacoast-social-media-settings',
        'settings'   => 'seacoast-twitter',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'seacoast-pinterest', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'seacoast_sanitize_text',
    ) );
    $wp_customize->add_control( 'seacoast-pinterest', array(
        'label' => __( 'Pinterest URL', 'loco_textdomain' ),
        'section' => 'seacoast-social-media-settings',
        'settings'   => 'seacoast-pinterest',
        'type'    => 'text',
    ) );


    $wp_customize->add_setting( 'seacoast-flickr', array(
        'type'    => 'theme_mod',
        'default' => '',
        'transport' => 'postMessage',
        'sanitize_callback' => 'seacoast_sanitize_text',
    ) );
    $wp_customize->add_control( 'seacoast-flickr', array(
        'label' => __( 'Flickr URL', 'loco_textdomain' ),
        'section' => 'seacoast-social-media-settings',
        'settings'   => 'seacoast-flickr',
        'type'    => 'text',
    ) );

    ///////////////////////////////////////////////////////////////////////////
    // Footer

    $wp_customize->add_section( 'seacoast-copyright-message-settings', array(
            'title'          => 'Contact Information',
            'priority'       => 200,
    ) );

    $wp_customize->add_setting( 'seacoast-company-name', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-company-name'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'seacoast_sanitize_text',
        ) );
    $wp_customize->add_control( 'seacoast-company-name', array(
            'label' => 'Company Name',
            'section' => 'seacoast-copyright-message-settings',
            'settings'   => 'seacoast-company-name',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'seacoast-address', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-address'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'seacoast_sanitize_text',
        ) );
    $wp_customize->add_control( 'seacoast-address', array(
            'label' => 'Address',
            'section' => 'seacoast-copyright-message-settings',
            'settings'   => 'seacoast-address',
            'type'    => 'text',
    ) );
    $wp_customize->add_setting( 'seacoast-town-state-zip', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-town-state-zip'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'seacoast_sanitize_text',
    ) );
    $wp_customize->add_control( 'seacoast-town-state-zip', array(
            'label' => 'Town, State Zipcode',
            'section' => 'seacoast-copyright-message-settings',
            'settings'   => 'seacoast-town-state-zip',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'seacoast-copyright-message', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-copyright-message'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'seacoast_sanitize_text',
        ) );
    $wp_customize->add_control( 'seacoast-copyright-message', array(
            'label' => 'Copyright Text',
            'section' => 'seacoast-copyright-message-settings',
            'settings'   => 'seacoast-copyright-message',
            'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'seacoast-mission-statement', array(
            'type'    => 'theme_mod',
            'default' => seacoast_theme_mod_default('seacoast-mission-statement'),
            'transport' => 'postMessage',
            'sanitize_callback' => 'seacoast_sanitize_text',
        ) );
    $wp_customize->add_control( 'seacoast-mission-statement', array(
            'label' => 'Mission Statement',
            'section' => 'seacoast-copyright-message-settings',
            'settings'   => 'seacoast-mission-statement',
            'type'    => 'textarea',
    ) );

}
add_action( 'customize_register', 'seacoast_customize_register' );


function seacoast_customize_css()
{
    // add customizer CSS to <head>
    ?>


    <style type="text/css">


        .navbar-default .navbar-nav > li > a {
          color: <?php echo seacoast_get_theme_mod( 'seacoast-color-nav-link', '#fff' ); ?>;
          font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-nav-menu', 'arial,helvetica')); ?>;
        }


        input[type="submit"], button {
            font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-button', 'arial,helvetica')); ?>;
        }
        .navbar-default .navbar-nav > li > a:hover {
            color: <?php echo seacoast_get_theme_mod( 'seacoast-color-text-link', '#aaa' ); ?>;
        }

        .navbar-default .navbar-nav .current-menu-item a {
            color: <?php echo seacoast_get_theme_mod( 'seacoast-color-nav-link', '#aaa' ); ?>;
            /*border-bottom: 2px solid <?php echo seacoast_get_theme_mod( 'seacoast-color-nav-link', '#aaa' ); ?>;*/
        }

        .navbar .navbar-header button .icon-bar {
          /*background-color: <?php echo seacoast_get_theme_mod( 'seacoast-color-text-link', '#aaa' ); ?>;*/
        }

        .navbar .navbar-header button .icon-title {
          color: <?php echo seacoast_get_theme_mod( 'seacoast-color-text-link', '#aaa' ); ?>;
        }


        body, #main {
            color: <?php echo seacoast_get_theme_mod( 'seacoast-color-text-body', '#000' ); ?>;
            font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-body-text', 'arial,helvetica')); ?>;
        }
        .site-header, .site-main {
          font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-body-text', 'arial,helvetica')); ?>;
        }
        a, a:visited, .site-main a, .site-main a:visited, .site-main a:active, .site-main a:hover, .site-main a:focus {
          color: <?php echo seacoast_get_theme_mod( 'seacoast-color-text-link', '#aaa' ); ?>;
        }
        .call-to-action {
          background-color: <?php echo seacoast_get_theme_mod( 'seacoast-color-call-to-action'); ?>;
        }
        h1, h2, h3, h4, h5, h6 {
            font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-heading', 'arial,helvetica')); ?>;
        }
        hr, .rule-lines {
          border-color: <?php echo seacoast_get_theme_mod( 'seacoast-color-rule-lines', '#aaa' ); ?>;
        }
        .site-main.col-md-8 {
          border-right-color: <?php echo seacoast_get_theme_mod( 'seacoast-color-rule-lines', '#aaa' ); ?>;
        }
        .widget-area.col-md-4 {
          border-left-color:  <?php echo seacoast_get_theme_mod( 'seacoast-color-rule-lines', '#aaa' ); ?>;
        }

        .button {
            font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-button', 'arial,helvetica')); ?>;
        }

        .wp-caption, .caption {
            font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-caption', 'arial,helvetica')); ?>;
        }
        #secondary {
        }

        .site-footer {
        }
        .site-footer a, .site-footer a .fa {
        }

        .site-footer a:focus, .site-footer a:hover, .site-footer a:active,
        .site-footer a .fa:focus, .site-footer a .fa:hover, .site-footer a .fa:active {
        }
        .site-footer {
            /*font-family: <?php echo seacoast_font_family( seacoast_get_theme_mod( 'seacoast-font-footer', 'arial,helvetica')); ?>;*/
        }

    </style>
    <?php

}
add_action( 'wp_head', 'seacoast_customize_css');


// change editor_style CSS dynamically for the admin editor
function seacoast_theme_editor_dynamic_styles( $mceInit ) {
    $styles = " \
    body.mce-content-body {  \
      background-color: ".seacoast_get_theme_mod( 'seacoast-body-backgroundcolor', '#fff' )."; \
      color:".seacoast_get_theme_mod( 'seacoast-body-textcolor', '#000' )."; \
      font-family:".seacoast_get_theme_mod( 'seacoast-font-body-text', 'arial,helvetica')."; \
      font-size: 14px; \
    } \
    body.mce-content-body h1, h2, h3, h4, h5, h6 { \
      font-family:".seacoast_get_theme_mod( 'seacoast-font-heading-text', 'arial,helvetica')."; \
    } \
    .wp-caption-dd { \
      color:".seacoast_get_theme_mod( 'seacoast-body-textcolor', '#000' )."; \
    } \
    ";

    if ( isset( $mceInit['content_style'] ) ) {
        $mceInit['content_style'] .= ' ' . $styles . ' ';
    } else {
        $mceInit['content_style'] = $styles . ' ';
    }
    return $mceInit;
}
add_filter('tiny_mce_before_init','seacoast_theme_editor_dynamic_styles');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function seacoast_customize_preview_js() {
    wp_enqueue_script( 'seacoast_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '4'.date( '.YmdGi'), true );
}

add_action( 'customize_preview_init', 'seacoast_customize_preview_js' );
