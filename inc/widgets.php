<?php
/**
 * SEACOAST Custom Widgets
 *
 * @package SEACOAST
 */
 /**
  * Add Seacoast_Harvest_Widget widget.
  */
 class Seacoast_Harvest_Widget extends WP_Widget {

     /**
      * Register widget with WordPress.
      *
      **/
     function __construct() {
         parent::__construct(
 			'seacoast_harvest_widget', // Base ID
 			__('SEACOAST HARVEST', 'seacoast'), // Name
 			array( 'description' => __( 'Seacoast Harvest', '' ), ) // Args
 		);
     }

     /**
 	 * Front-end display of widget.
 	 *
 	 * @see WP_Widget::widget()
 	 *
 	 * @param array $args     Widget arguments.
 	 * @param array $instance Saved values from database.
 	 */
 	public function widget( $args, $instance ) {
 		$title = apply_filters( 'widget_title', $instance['title'] );

 		echo $args['before_widget'];
 		if ( ! empty( $title ) ) {
 			echo $args['before_title'] . $title . $args['after_title'];
 		}

 		//echo __( 'Default Content', '' );
 		//echo '<br />SH FORM';
 		?>
 		<div class="seacoast-harvest-widget-form">
 			<form class="searchformx form-inline" role="form search" method="get" id="searchform" action="https://seacoastharvest.org" target="_blank">
 			  <div class="form-group text-right">
 			    <input type="text" class="form-control" style="width:208px;" name="s" id="s" value="" placeholder="Search for produce, farms.">
 			  </div>
 			    <button type="submit" class="btn btn-default">Search</span></button>
 			</form>
 		</div>
 		<?php

 		echo $args['after_widget'];
 	}

     /**
 	 * Back-end widget form.
 	 *
 	 * @see WP_Widget::form()
 	 *
 	 * @param array $instance Previously saved values from database.
 	 */
 	public function form( $instance ) {
 		if ( isset( $instance[ 'title' ] ) ) {
 			$title = $instance[ 'title' ];
 		}
 		else {
 			$title = __( 'Seacoast Harvest', '' );
 		}
 		?>
 		<p>
 		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
 		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
 		</p>
 		<?php
 	}

     /**
 	 * Sanitize widget form values as they are saved.
 	 *
 	 * @see WP_Widget::update()
 	 *
 	 * @param array $new_instance Values just sent to be saved.
 	 * @param array $old_instance Previously saved values from database.
 	 *
 	 * @return array Updated safe values to be saved.
 	 */
 	public function update( $new_instance, $old_instance ) {
 		$instance = array();
 		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

 		return $instance;
 	}
 }

 add_action( 'widgets_init', create_function( '', "register_widget( 'Seacoast_Harvest_Widget' );" ) );



/**
 * Add Sponsor widget.
 */
class Sponsor_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     *
     **/
    function __construct() {
        parent::__construct(
			'sponsor_widget', // Base ID
			__('SPONSORS', 'seacoast'), // Name
			array( 'description' => __( 'Sponsor list.', '' ), ) // Args
		);
    }

    /**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
		$term = $instance['term'];

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
    ?>
    <p>
    <div class="row sponsors">
      <?php
        $args = array( 'post_type'=>'sponsor',
                       'nopaging'=> true,
                       'tax_query' => array(
                           array(
                               'taxonomy' => 'program',
                               'field' => 'slug',
                               'terms' => $term,
                           )
                       ),
                       'orderby' => 'title', 'order' => 'ASC'  );
        $the_query = new WP_Query( $args);

        if( $the_query->have_posts() ) {
         while ( $the_query->have_posts() )
          {
              global $post;
              $the_query->the_post();
              $url = get_field( 'url', $post->ID);

              $i = get_field( 'logo');
              //var_dump( $i);
              $alt = $i['alt'];
              $size = 'sponsor';
              $thumb = $i['sizes'][ $size ];
              $width = $i['sizes'][ $size . '-width' ];
              $height = $i['sizes'][ $size . '-height' ];
              ?>

              <div class="col-xs-12 text-center sponsor">
                <?php if ( $url): ?>
                  <a href="<?php echo $url; ?>" target="_blank">
                <?php endif; ?>
                  <img class="img-responsive logo" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                <?php if ( $url): ?>
                  </a>
                <?php endif; ?>
              </div>

            <?php
          }
        }
      ?>
    </div>
    </p>

		<?php

		echo $args['after_widget'];
	}

    /**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'Sponsor', '' );
		}
    if ( isset( $instance[ 'term' ] ) ) {
			$term = $instance[ 'term' ];
		}
		else {
			$term = __( 'winter-farmers-market', '' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
    <!-- <p>
		<label for="<?php echo $this->get_field_id( 'term' ); ?>"><?php _e( 'Term:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'term' ); ?>" name="<?php echo $this->get_field_name( 'term' ); ?>" type="text" value="<?php echo esc_attr( $term ); ?>">
		</p> -->
    <p>
      <label for="<?php echo $this->get_field_id( 'term' ); ?>"><?php _e( 'Program:' ); ?></label>
      <select id="<?php echo $this->get_field_id('term'); ?>" name="<?php echo $this->get_field_name('term'); ?>" class="widefat" style="width:100%;">
          <?php foreach( get_terms( array( 'taxonomy' => 'program')) as $term) { ?>
              <option <?php selected( $instance['term'], $term->slug ); ?> value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
          <?php } ?>
      </select>
    </p>
		<?php

	}

    /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
    $instance = array();
		//$instance = $old_instance;
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['term'] = ( ! empty( $new_instance['term'] ) ) ? strip_tags( $new_instance['term'] ) : '';

		return $instance;
	}
}

add_action( 'widgets_init', create_function( '', "register_widget( 'Sponsor_Widget' );" ) );



class Custom_Blog_Post extends WP_Widget {
	function __construct() {
		parent::__construct( false, 'SEACOAST Blog Post');
	}

function form($instance) {

		if ( isset( $instance[ 'title' ] ) )
		{
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'seacoast' );
		}

		// outputs the options form on admin
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'seacoast' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<br /></p>
		<?php
	}

function update($new_instance, $old_instance) {

		// processes widget options to be saved
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
//		$instance['body'] = $new_instance['body'];

		return $instance;
	}

function widget($args, $instance) {
		// outputs the content of the widget

        $title = $instance['title'];
//        $body = $instance['body'];

        $s = '';

				$template_url = home_url();
        $upload_url = home_url() . '/wp-content/uploads/';

        ?>
	<hr class="home" />
	<?php /* Start the Loop */


        // Custom Image_Text_Link script
        //$s .=
        //'<div id="secondary" class="widget-area custom-sidebar col-xs-12 col-sm-12 col-md-12" role="complementary">
				//'<div class="textwidgetx seacoast-custom-postx">';

        if ( $title)
            $s .= '<a href="blog"><h2>'.$title.'</h2></a>';

        $args = array( 'post_type'=>'post',
                       'nopaging'=> true,
                       'orderby' => 'date', 'order' => 'DESC'  );
        $the_query = new WP_Query( $args);
        echo $s;
        $s = '';

        // just show the first one.
        if ( $the_query->have_posts() )
        	if ($the_query->have_posts())
        	{
        	    $the_query->the_post();
  //              $s .= '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
//                $s .= wpautop( get_the_content()).'<br />';
        	}


					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'excerpt-home' );


        //$s .= '</div>';
//        $s .= '</div><br />';

  //      echo $s;
	}


}
register_widget('Custom_Blog_Post');

?>
