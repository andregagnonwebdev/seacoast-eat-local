<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package seacoast
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'row'); ?>>

	<?php $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); ?>
	<?php if ( ! empty( $featured_image_url )):	?>
		<div class="col-xs-3 col-md-1">
			<?php the_post_thumbnail(); ?>
		</div>
	<?php endif; ?>

	<?php if ( ! empty( $featured_image_url )):	?>
		<div class="col-xs-9 col-md-11">
	<?php else: ?>
		<div class="col-xs-12">
	<?php endif; ?>

	<header class="entry-headerx">
		<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php //seacoast_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php if ( 'post' === get_post_type() ) : ?>
			<?php seacoast_posted_on_search(); ?>
		<?php endif; ?>
		<?php echo str_replace( '<p>', '', get_the_excerpt()) ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footerx">
		<?php //seacoast_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</div>
</article><!-- #post-## -->
