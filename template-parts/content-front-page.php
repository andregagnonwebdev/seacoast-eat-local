<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package seacoast
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->



	<div class="entry-content row">
		<?php
		// check if the repeater field has rows of data
		if( have_rows('cards') ):
			$count = 1;

		 	// loop through the rows of data
		    while ( have_rows('cards') ) : the_row();
					// display a sub field value
					?>
						<?php $p = get_sub_field( 'page_or_post'); ?>
						<?php if ( $p) $l = get_permalink( $p->ID); ?>
						<div class="col-xs-12 col-sm-6 col-md-4 card">
							<div class="image">
								<a href="<?php echo $l; ?>">
									<?php
										$i = get_sub_field( 'image');
										//var_dump( $i);
										$alt = $i['alt'];

										$size = 'home-card';
										$thumb = $i['sizes'][ $size ];
										$width = $i['sizes'][ $size . '-width' ];
										$height = $i['sizes'][ $size . '-height' ];
									?>
									<img class="img-responsive" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
									<!-- <img class="img-responsive" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>"  /> -->
								</a>
							</div>
							<h2><?php the_sub_field('title'); ?></h2>
							<p><?php the_sub_field('subtitle'); ?></p>
							<!-- <a href="<?php the_sub_field('link'); ?>">
								<?php the_sub_field('link_title'); ?>
							</a> -->
							<?php if ( $p ): ?>
								<a href="<?php echo $l; ?>"><?php the_sub_field('link_title'); ?></a>
							<?php endif; ?>
						</div>
						<?php if ( $count % 2 == 0): ?>
							<div class="visible-sm">
								<div class="clearfix"></div>
							</div>
						<?php endif; ?>
						<?php if ( $count % 3 == 0): ?>
							<div class="visible-md visible-lg">
								<div class="clearfix"></div>
							</div>
						<?php endif; ?>
						<?php $count++; ?>
					<?php
		    endwhile;

		else :

		    // no rows found

		endif;

		?>
		<div class="col-xs-12 col-md-8">
			<?php	the_content(); ?>
		</div>
		<?php	get_sidebar('home'); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
