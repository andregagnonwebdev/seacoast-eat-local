<?php
/**
 * Template part for displaying social links
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package seacoast
 */

?>

<div class="social">
  <?php if ( $s = seacoast_get_theme_mod( 'seacoast-facebook')): ?>
    <a href="<?php echo $s ?>" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a>
  <?php endif; ?>
  <?php if ( $s = seacoast_get_theme_mod( 'seacoast-instagram')): ?>
    <a href="<?php echo $s ?>" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a>
  <?php endif; ?>
  <?php if ( $s = seacoast_get_theme_mod( 'seacoast-twitter')): ?>
    <a href="<?php echo $s ?>" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a>
  <?php endif; ?>
  <?php if ( $s = seacoast_get_theme_mod( 'seacoast-pinterest')): ?>
    <a href="<?php echo $s ?>" target="_blank" title="pinterest"><i class="fa fa-pinterest"></i></a>
  <?php endif; ?>
  <?php if ( $s = seacoast_get_theme_mod( 'seacoast-flickr')): ?>
    <a href="<?php echo $s ?>" target="_blank" title="flickr"><i class="fa fa-flickr"></i></a>
  <?php endif; ?>
</div>
