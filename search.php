<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package seacoast
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main col-xs-12 col-md-12" role="main">
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php get_search_form( true); ?>
				<h2 class="page-title">
					<?php global $wp_query; echo '('. $wp_query->found_posts . ')'; ?>
					<?php printf( esc_html__( 'Search Results for: %s', 'seacoast' ), '<span>' . get_search_query() . '</span>' ); ?>
				</h2>
			</header><!-- .page-header -->


			<?php
				global $query_string;
				//query_posts( $query_string . '&posts_per_page=-1' );
			?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation(
				array(
            'prev_text'                  => __( 'More Search Results' ),
            'next_text'                  => __( 'Previous Search Results' ),
        )
			);

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
//get_sidebar();
get_footer();
